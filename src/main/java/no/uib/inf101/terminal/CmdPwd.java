package no.uib.inf101.terminal;

public class CmdPwd implements Command {

    Context context;

    @Override
    public String run(String[] args) {
        return this.context.getCwd().getAbsolutePath();
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public void setContext(Context context) {
        Command.super.setContext(this.context);
    }
}