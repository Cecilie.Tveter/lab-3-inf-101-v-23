package no.uib.inf101.terminal;

import java.io.File;

public class CmdLs implements Command{

    Context context;

    @Override
    public String run(String[] args) {
        File cwd = this.context.getCwd();
        String s = "";
        for (File file : cwd.listFiles())
            s += file.getName();
        
        return null;
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public void setContext(Context context) {
        Command.super.setContext(this.context);
    }
}
