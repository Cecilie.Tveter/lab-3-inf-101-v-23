package no.uib.inf101.terminal;

public class CmdEcho implements Command {
    @Override
    public String run(String[] args) {
        String string = "";
        for (String a : args) {
            string += String.join(" ", a + " ");
        }
        return string;
    }

    @Override
    public String getName() {
        return "echo";
    }
}
